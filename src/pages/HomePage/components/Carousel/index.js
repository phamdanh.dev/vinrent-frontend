import React from "react";
import { Carousel } from "antd";
import "./style.scss";
import nha1 from "../../../../assets/images/carousel/nha1.jpg";
import nha2 from "../../../../assets/images/carousel/nha2.jpeg";
import nha3 from "../../../../assets/images/carousel/nha3.jpg";
import nha4 from "../../../../assets/images/carousel/nha4.jpg";
import AppSearchTools from "../SearchTools";

const AppCarousel = React.memo(() => {
  return (
    <div style={{ position: "relative" }}>
      <Carousel autoplay dotPosition={"top"}>
        <div>
          <img src={nha1} alt="" />
        </div>
        <div>
          <img src={nha2} alt="" />
        </div>
        <div>
          <img src={nha3} alt="" />
        </div>
        <div>
          <img src={nha4} alt="" />
        </div>
      </Carousel>
      <AppSearchTools />
    </div>
  );
});

export default AppCarousel;
